IMAGE=registry.gitlab.com/kamackay/markdown:$1

time docker build . -t "$IMAGE" && \
    # docker-squash "$IMAGE" --tag "$IMAGE" && \
    docker push "$IMAGE" && \
    kubectl --context do-nyc3-keithmackay-cluster -n markdown \
      set image ds/markdown-ui server=$IMAGE

sleep 1
ATTEMPTS=0
ROLLOUT_STATUS_CMD="kubectl --context do-nyc3-keithmackay-cluster rollout status ds/markdown-ui -n markdown"
until $ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq 60 ]; do
  $ROLLOUT_STATUS_CMD
  ATTEMPTS=$((ATTEMPTS + 1))
  sleep 1
done

ECHO "Successfully deployed" $1
