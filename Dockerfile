FROM node:latest as builder

WORKDIR /root

ADD package.json ./

RUN yarn install

ENV PATH /root/node_modules/.bin:$PATH

COPY ./ ./

RUN yarn build

FROM kamackay/nginx:latest

COPY --from=builder /root/build /www/

COPY ./nginx.conf /etc/nginx/nginx.conf
